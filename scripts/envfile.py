#!/usr/bin/env python3

from multiprocessing import cpu_count
from os import getenv
from shlex import split
from subprocess import DEVNULL, PIPE, run
from typing import Iterable


def _run(cmd: str) -> str:
    return run(
        split(cmd), stdin=DEVNULL, stdout=PIPE, text=True, check=True
    ).stdout.strip()


def env() -> Iterable[str]:
    bob_py_version = _run("hatch version")
    if getenv("CI_COMMIT_TAG") is not None:
        bob_rpm_version = bob_py_version
    else:
        bob_rpm_version = getenv("CI_COMMIT_REF_NAME", "unknown")

    variables = []
    variables.append(f"BOB_PY_VERSION={bob_py_version}")
    variables.append(f"BOB_JOBS={cpu_count()}")

    if getenv("CI_COMMIT_TAG") is not None:
        variables.append(f"BOB_RPM_VERSION={bob_py_version}")
    else:
        variables.append(f"BOB_RPM_VERSION={bob_rpm_version}")

    variables.append(f"BOB_PY_WHEEL=bob-{bob_py_version}-py3-none-any.whl")
    variables.append(f"BOB_PY_ARCHIVE=bob-{bob_py_version}.tar.gz")
    variables.append(f"BOB_RPM=bob-{bob_rpm_version}-1.noarch.rpm")

    def add_newline(s: str) -> str:
        return f"{s}\n"

    return map(add_newline, variables)


if __name__ == "__main__":
    import argparse
    import pathlib

    argument_parser = argparse.ArgumentParser(
        description="Generate an environment file for Gitlab CI"
    )
    argument_parser.add_argument(
        "path",
        type=pathlib.Path,
        default=pathlib.Path("ci.env"),
        nargs="?",
        help="Path to output file",
    )
    parsed_args = argument_parser.parse_args()

    with parsed_args.path.open("wt") as fp:
        fp.writelines(env())
