#!/usr/bin/env python3

from os import environ
from pathlib import Path
from shlex import split
from string import Template
from subprocess import DEVNULL, run
from tempfile import NamedTemporaryFile
from textwrap import dedent

spec_template = Template(
    dedent(
        """
    Name:           bob
    Version:        $bob_version
    Release:        1
    Summary:        Simple declarative Linux sysroot builder.

    %global archivename bob-%{version}

    License:        MIT
    URL:            https://gitlab.com/gduque96/bob
    Source0:        %{url}/-/archive/%{version}/%{archivename}.tar.gz

    BuildArch:      noarch

    BuildRequires:  python3-devel
    BuildRequires:  pyproject-rpm-macros
    BuildRequires:  python3dist(tomli)
    BuildRequires:  python-hatchling

    Requires:       python-jinja2

    %description
    Simple declarative Linux sysroot builder.

    %prep
    %autosetup -n %{archivename} -p1

    %build
    %pyproject_wheel

    %install
    %pyproject_install
    %pyproject_save_files bob

    %files -f %{pyproject_files}
    %license LICENSE.txt
    %{_bindir}/bob

    %changelog
    * Sun Aug 07 2022 Gabriel Duqué <gabriel@duque.bzh>
    - Initial package.
"""
    )
)


def _run(cmd: str) -> None:
    run(split(cmd), stdin=DEVNULL, check=True)


def rpmbuild(spec: Path) -> None:
    _run("rpmdev-setuptree")
    _run(f"rpmdev-spectool --get-files --sourcedir '{spec}'")
    _run(f"dnf builddep -y '{spec}'")
    _run(f"rpmbuild -bb --define '_rpmdir {Path().absolute()}/rpm' '{spec}'")


if __name__ == "__main__":
    with NamedTemporaryFile("wt", suffix=".spec") as fp:
        fp.write(
            spec_template.substitute(bob_version=environ["BOB_RPM_VERSION"])
        )
        fp.flush()
        rpmbuild(Path(fp.name).absolute())
