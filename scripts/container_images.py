#!/usr/bin/env python3

import os
import shlex
import shutil
import subprocess


class ImageBuilder:
    @staticmethod
    def check_dependencies() -> None:
        deps = ("buildah", "dnf")
        for dep in deps:
            if shutil.which(dep) is None:
                raise RuntimeError(f"Failed to find dependency: {dep}")

    @staticmethod
    def run(cmd: str) -> str:
        return subprocess.run(
            shlex.split(cmd), stdout=subprocess.PIPE, check=True, text=True
        ).stdout.rstrip("\r\n")

    def __call__(self) -> None:
        platforms = (
            ("x86_64", "amd64"),
            ("aarch64", "arm64"),
            ("ppc64le", "ppc64le"),
            ("s390x", "s390x"),
        )

        creds = shlex.quote(
            ":".join(
                (
                    os.environ["BOB_REGISTRY_USER"],
                    os.environ["BOB_REGISTRY_TOKEN"],
                )
            )
        )

        image = "quay.io/gduque/bob"
        manifest = f"{image}:latest"

        self.run(f"buildah manifest create {manifest}")

        for dnfarch, goarch in platforms:
            container = shlex.quote(self.run("buildah from scratch"))
            mountpoint = self.run(f"buildah mount {shlex.quote(container)}")
            name = shlex.quote(f"{image}:latest-{goarch}")
            self.run(
                " ".join(
                    (
                        "dnf install -y",
                        f"--installroot {shlex.quote(mountpoint)}",
                        "--releasever /",
                        f" --forcearch {shlex.quote(dnfarch)}",
                        "--nodocs",
                        "--setopt install_weak_deps=False",
                        f"./rpm/noarch/{os.environ['BOB_RPM']}",
                    )
                )
            )
            self.run(f"buildah umount {container}")
            self.run(f"buildah commit --squash {container} {name}")
            self.run(f"buildah push --creds={creds} {name} docker://{name}")
            self.run(f"buildah manifest add --arch {goarch} {manifest} {name}")

        self.run(
            " ".join(
                (
                    "buildah manifest push",
                    f"--creds {creds}",
                    "--format oci",
                    f"--all {manifest}",
                    f"docker://{manifest}",
                )
            )
        )


if __name__ == "__main__":
    image_builder = ImageBuilder()
    image_builder.check_dependencies()
    image_builder()
