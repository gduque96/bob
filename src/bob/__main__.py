# SPDX-FileCopyrightText: 2022-present Gabriel Duqué <gabriel@duque.bzh>
#
# SPDX-License-Identifier: MIT

import sys

if __name__ == "__main__":
    from bob.cli import bob

    sys.exit(bob())
