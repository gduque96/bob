# SPDX-FileCopyrightText: 2022-present Gabriel Duqué <gabriel@duque.bzh>
#
# SPDX-License-Identifier: MIT


def bob() -> int:
    print("Bob the builder!")
    return 0
